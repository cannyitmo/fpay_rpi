from picamera import PiCamera
from time import sleep
from time import time
import math

MAX_SAMPLES = 90
SAMPLE_PERIOD = 1

camera = PiCamera()
camera.rotation = 180

camera.start_preview(alpha=200)
entry_cnt = 0

while True:
    camera.capture('capture_buffer/sample_' + str(entry_cnt) + ".jpg")
    print("captured sample" + str(entry_cnt))
    entry_cnt = entry_cnt + 1
    if entry_cnt == MAX_SAMPLES:
        entry_cnt = 0
    pointer_file = open("capture_buffer/pointer.txt", "w+")
    pointer_file.truncate(0)
    pointer_file.write(str(entry_cnt))
    pointer_file.close()
    sleep(SAMPLE_PERIOD)

camera.stop_preview()
