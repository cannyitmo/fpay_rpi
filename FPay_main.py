import json
import struct
import sys
from bluepy.btle import UUID, Peripheral,DefaultDelegate
import boto3
import pprint


SatelliteData = list()


class MyDelegate(DefaultDelegate):
    #Constructor (run once on startup)
    def __init__(self, params):
        DefaultDelegate.__init__(self)

    #func is caled on notifications
    def handleNotification(self, cHandle, data):
#         print ("Notification from Handle: 0x" + format(cHandle,'02X') + " Value: "+ format(ord(data[0])))
         SatelliteData.extend(data)

# Initialisation  -------
button_service_uuid = UUID(0x2565)
button_char_uuid    = UUID(0x8933)

p = Peripheral("c4:f3:b6:9f:49:4d","random")
print("Peripheral created")
p.setDelegate( MyDelegate(p) )

#Get SatelliteService
SatelliteService=p.getServiceByUUID(button_service_uuid)
# Get The Button-Characteristics
SatellitC=SatelliteService.getCharacteristics(button_char_uuid)[0]
#Get The handle tf the  Button-Characteristics
hSatellitC=SatellitC.getHandle()
# Search and get Get The Button-Characteristics "property" (UUID-0x2902 CCC-Client Characteristic Configuration))
#  wich is located in a handle in the range defined by the boundries of the SatelliteService
for desriptor in p.getDescriptors(hSatellitC,0x00F):  # The handle range should be read from the services
   if (desriptor.uuid == 0x2902):                   #      but is not done due to a Bluez/BluePy bug :(
        print ("Client Characteristic Configuration found at handle 0x"+ format(desriptor.handle,"02X"))
        hSatellitCCC=desriptor.handle

p.writeCharacteristic(hSatellitCCC, struct.pack('<bb', 0x01, 0x00))
print "Notification is turned on"

pp = pprint.PrettyPrinter(indent=4)

s3 = boto3.resource('s3')

filename = ''
bucket_name = 'fpaytestbucket'



while True:
    if p.waitForNotifications(1.0):
        # handleNotification() was called
#        print(format(ord(SatelliteData[0])))
        if format(ord(SatelliteData[0])) == '1':
	    #push the last 5 pictures and compare
            output = []
            theFile = open("capture_buffer/pointer.txt", "r")
            theInts = []
            for val in theFile.read().split():
                theInts.append(float(val))
            theFile.close()
            if not val:
                print("Read error")
                continue
#            print(val)
            if len(val) == 1:
                pointer_value = int(val[0])
            elif len(val) == 2:
                pointer_value = 10 * int(val[0]) + int(val[1])
            else:
                print("Pointer value length error")

            if pointer_value > 90:
                print("Pointer value error")
	    pointer_value = pointer_value - 1

#            for x in range(0, 5):
            if True:
                x = 0
                if(pointer_value + x >= 90):
                    pointer_value = 0
                    x = pointer_value + x - 89
                print("Pointer value: " + str(pointer_value))
                filename = "sample_" + str(pointer_value) + ".jpg"
                upload = s3.Bucket(bucket_name).upload_file("capture_buffer/" + filename, "sample_" + str(x) + ".jpg")
		print("File uploaded")
                client = boto3.client('rekognition')
                bucket = 'fpaytestbucket'
#                print(str(x))
                response = client.compare_faces(
                    SimilarityThreshold=90,
                    SourceImage={
                        'S3Object': {
                            'Bucket': bucket,
                            'Name': "sample_" + str(x) + ".jpg",
                        },
                    },
                    TargetImage={
                        'S3Object': {
                            'Bucket': bucket,
                            'Name': 'source_sample.jpg',
                        },
                    },
                )
#                pp.pprint(response)
                loc_resp = response['FaceMatches']
		if len(loc_resp) > 0:
#                if int(loc_resp[0].get("Similarity")) > 90:
                    print("Ebalo Sovpalo") 
		else:
		    print("Ne Sovpalo")

        del SatelliteData[:]
        continue

    #Now we have value in SatelliteData

